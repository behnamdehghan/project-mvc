<?php

namespace App\Http\Controllers\Admin;

use App\Category;


class CategoryController extends AdminController
{

    public function index()
    {
        $categories = Category::whereNull('parent_id')->get();
        return view('admin.category.index', compact('categories'));    
    }
    public function create()
    {
        $categories = Category::all();
        return view('admin.category.create', compact('categories'));    
    }
    

    public function store()
    {
    }

    public function edit($id)
    {
    }

    public function update()
    {
    }
    public function destroy()
    {
    }
}
