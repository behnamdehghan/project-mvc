<?php

function sidebarActive($url, $contain = true){
    if($contain)
    return (strpos(currentUrl(), $url) === 0) ? 'active' : '';
    else
    return  $url === currentUrl() ? 'active' : '';
}