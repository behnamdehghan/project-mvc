<?php

namespace System\Session;

class Session
{
    public  function setMethod($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    public  function getMethod($name)
    {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : false;
    }

    public static function removeMethod($name)
    {
        if(isset($_SESSION[$name]))
        unset($_SESSION[$name]);
    }

    public static function __callStatic($name,$arguments)
    {
        $instance = new self();
        return $instance->methodCaller($name, $arguments);
    }
    private function methodCaller($method, $arguments)
    {
        $suffix = 'Method';
        $methodName = $method.$suffix;
        return call_user_func_array(array($this, $methodName), $arguments);
    }
}